import json
import unittest

from papabot.utils import group_promos
from src.papabot.promos import Promo


class PromoTestCase(unittest.TestCase):
    def test_it_parses_correctly(self):
        d = {
            "code": "foo",
            "name": "12 - foo, bar - 15% off - От 24.55 рублей - Minsk, Gomel",
        }

        p = Promo(**d)
        self.assertEqual(p.code, d["code"])
        self.assertEqual(p.name, d["name"])

        self.assertEqual(p.aliases, ["foo", "bar"])
        self.assertEqual(p.description, "15% off")
        self.assertEqual(p.condition, "От 24.55 рублей")
        self.assertEqual(p.min_order, 24.55)
        self.assertEqual(p.locations, ["Minsk", "Gomel"])

    def test_promos_grouping(self):
        foo = Promo(
            **{
                "code": "foo",
                "name": "12 - foo, bar - 15% off - От 24.55 рублей - Minsk, Gomel",
            }
        )
        bar = Promo(
            **{
                "code": "bar",
                "name": "12 - foo, bar - 15% off - От 24.55 рублей - Minsk, Gomel",
            }
        )
        baz = Promo(
            **{
                "code": "baz",
                "name": "12 - foo, bar - 15% off - От 29.55 рублей - Minsk, Vitebsk",
            }
        )
        promos = [baz, foo, bar]

        groups = group_promos(promos)
        self.assertEqual(len(groups), 2)

        self.assertListEqual([[bar, foo], [baz]], groups)

    def test_no_promos_are_lost_after_grouping(self):
        with open("promos_snapshot.json", "r") as f:
            promos_data = json.load(f)
        promos = [Promo(**p) for p in promos_data]

        groups = group_promos(promos)
        self.assertEqual(len(promos), sum(len(group) for group in groups))

        promos_data_set = set()
        for group in groups:
            for promo in group:
                promos_data_set.add((promo.code, promo.name))

        self.assertSetEqual(set((p.code, p.name) for p in promos), promos_data_set)
