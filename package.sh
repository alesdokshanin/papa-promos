#!/usr/bin/env bash
set -e

rm -rf /out/*
rm -rf /tmp/*

mkdir -p /out
mkdir -p /tmp/lambda

echo "Installing dependencies..."
poetry install

site_packages_dir=$(poetry run python -c 'import sys;print("\n".join(sys.path))' | grep 'site-packages$')
cp -r $site_packages_dir/* /tmp/lambda/

echo "OK - dependencies are installed successfully"

# copy the code
cp -r src/* /tmp/lambda/

cd /tmp/lambda

# remove pyc files
find . -name "*.pyc" -exec rm -f {} \;

echo "Archiving the code to create a lambda package..."
zip -q -r9 /out/package.zip *
echo "OK - finished"