#!/bin/sh
set -euxo pipefail

VENV_DIR=$(poetry run env | grep 'VIRTUAL_ENV=' | cut -d '=' -f2)
mkdir -p /venv
cp -r $VENV_DIR/* /venv/