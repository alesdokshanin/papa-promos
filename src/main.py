import logging

from telegram import Bot, Update
from telegram.ext import Dispatcher, Updater
from telegram.utils.request import Request

import papabot.config
from papabot.handlers.setup import setup_handlers


def lambda_handler(event, *_, **__) -> dict:
    update_json = event
    update = Update.de_json(update_json, bot=bot)
    dispatcher.process_update(update=update)
    return {"statusCode": 200, "message": "UHN TISS UHN TISS UHN TISS ♬♪ ᕕ(⌐■_■)ᕗ ♪♬"}


is_in_updater_mode = __name__ == "__main__"

config = papabot.config.Config()

bot = Bot(config.telegram_token, request=Request(con_pool_size=8))

logging.basicConfig(
    level=logging.INFO, format="%(asctime)s - %(levelname)s - %(name)s - %(message)s"
)
logger = logging.getLogger()
logger.setLevel(logging.INFO)

if is_in_updater_mode:
    updater = Updater(token=config.telegram_token, use_context=True)
    dispatcher = updater.dispatcher
else:
    updater = None
    dispatcher = Dispatcher(bot, update_queue=None, workers=0, use_context=True)

setup_handlers(dispatcher)


if is_in_updater_mode:
    updater.start_polling()
    updater.idle()
