import functools

from papabot.config import config
from papabot.utils import log_users_visit


def log_visit(handler):
    @functools.wraps(handler)
    def wrapped(*args, **kwargs):
        update = args[0]
        log_users_visit(user=update.effective_user)
        return handler(*args, **kwargs)

    return wrapped


def admins_only(handler):
    @functools.wraps(handler)
    def wrapped(*args, **kwargs):
        user = args[0].effective_user
        if str(user.id) in config.admin_ids:
            return handler(*args, **kwargs)

    return wrapped
