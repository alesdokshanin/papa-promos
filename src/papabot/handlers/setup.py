from telegram.ext import CommandHandler, Dispatcher

from papabot.handlers.city import minsk
from papabot.handlers.other import hello, users


def setup_handlers(dispatcher: Dispatcher):
    handlers = [
        CommandHandler("hello", hello),
        CommandHandler("minsk", minsk),
        CommandHandler("users", users),
    ]
    for handler in handlers:
        dispatcher.add_handler(handler)
