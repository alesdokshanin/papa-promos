from datetime import datetime

from telegram import Update
from telegram.ext import CallbackContext

from papabot.cache import redis_client
from papabot.handlers.decorators import admins_only
from papabot.utils import grouper


@admins_only
def hello(update: Update, context: CallbackContext):
    msg = "Hello, {}".format(update.message.from_user.first_name)
    update.message.reply_text(msg)


@admins_only
def users(update: Update, *_):
    rows = redis_client.zrevrange("users", 0, 50, withscores=True)
    all_lines = [
        "{info} -- {time}".format(
            info=r[0].decode(), time=datetime.utcfromtimestamp(r[1]).isoformat()
        )
        for r in rows
    ]

    for lines in grouper(all_lines, n=25):
        msg = "\n".join((l for l in lines if l is not None))
        update.message.reply_text(msg)
