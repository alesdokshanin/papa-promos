from telegram import ParseMode, Update
from telegram.ext import CallbackContext

from papabot.handlers.decorators import log_visit
from papabot.promos import get_valid_promos, Promo
from papabot.utils import group_promos, grouper, promo_group_to_human


def _is_available_in_city(promo: Promo, city: str) -> bool:
    locations_to_try = ["Все города", city]
    return any(
        (loc.lower() in (x.lower() for x in promo.locations) for loc in locations_to_try)
    )


@log_visit
def minsk(update: Update, context: CallbackContext):
    all_promos = get_valid_promos()
    minsk_promos = (p for p in all_promos if _is_available_in_city(promo=p, city="Минск"))

    all_lines = [promo_group_to_human(p) + "\n" for p in group_promos(minsk_promos)]

    for lines in grouper(all_lines, n=10):
        msg = "\n".join((l for l in lines if l is not None))
        update.message.reply_text(msg, parse_mode=ParseMode.MARKDOWN)
