from pydantic import BaseModel, BaseSettings

__all__ = ("config",)


class RedisConfig(BaseModel):
    host: str
    port: int


class Config(BaseSettings):
    telegram_token: str
    redis: RedisConfig
    admin_ids = ["145729930"]

    class Config:
        env_prefix = "app_"


config = Config()
