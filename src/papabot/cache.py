import redis

from papabot.config import config

__all__ = ("redis_client",)

redis_client = redis.Redis(host=config.redis.host, port=config.redis.port)
