import dataclasses
import json
from typing import List, Optional

import redis

from papabot.promos.promo import Promo


class PromoCacheManager:
    cache_key = "promocodes"
    cache_ttl = 12 * 60 * 60  # 12 hours

    def __init__(self, redis_client: redis.Redis):
        self.redis_client = redis_client

    def get_promos(self) -> Optional[List[Promo]]:
        cache_payload = self.redis_client.get(self.cache_key)
        if cache_payload is None:
            return None

        raw_promos = json.loads(cache_payload)
        promos = [Promo(**p_data) for p_data in raw_promos]
        return promos

    def cache_promos(self, promos: List[Promo]) -> None:
        json_data = json.dumps([dataclasses.asdict(p) for p in promos])
        self.redis_client.set(self.cache_key, json_data, ex=self.cache_ttl)
