from __future__ import annotations

from typing import TYPE_CHECKING, List

from papabot.cache import redis_client
from papabot.promos.cache import PromoCacheManager
from papabot.promos.fetch import fetch_valid_promos

if TYPE_CHECKING:
    from papabot.promos import Promo


def get_valid_promos() -> List[Promo]:
    cache_mgr = PromoCacheManager(redis_client=redis_client)

    promos = cache_mgr.get_promos()
    if promos is None:
        promos = fetch_valid_promos()
        cache_mgr.cache_promos(promos=promos)

    return promos
