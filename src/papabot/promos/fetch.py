import asyncio
import http
import logging
from http import HTTPStatus
from typing import List

import aiohttp
from aiohttp import TCPConnector

from papabot.promos.promo import Promo

__all__ = ("fetch_valid_promos",)

logger = logging.getLogger(__name__)


def fetch_valid_promos() -> List[Promo]:
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    return loop.run_until_complete(_get_valid_codes_data())


async def _fetch_promos(cs: aiohttp.ClientSession) -> List[Promo]:
    response = await cs.get("https://www.papajohns.by/api/stock/codes")
    body = await response.json(content_type=None)
    return [Promo(code=p["code"], name=p["name"]) for p in body["codes"]]


async def _check_if_valid(code: str, cs: aiohttp.ClientSession) -> bool:
    url = "https://api.papajohns.by/stock/apply"
    r = await cs.post(
        url,
        headers={"X-Requested-With": "XMLHttpRequest"},
        json={
            "lang": "ru",
            "city_id": "1",
            "platform": "web",
            "stock_code": code,
            "gifts": []
        }
    )

    return r.status == HTTPStatus.OK


async def _get_valid_codes_data() -> List[Promo]:
    cs = aiohttp.ClientSession(connector=TCPConnector(limit=15))

    try:
        promos = await _fetch_promos(cs)
        promos_by_code = {p.code: p for p in promos}

        logger.debug(f"Found {len(promos)} codes")

        valid_promos = []

        fut_by_code = {
            c: asyncio.ensure_future(_check_if_valid(c, cs))
            for c in promos_by_code.keys()
        }
        await asyncio.wait(fut_by_code.values())

        for code, fut in fut_by_code.items():
            if fut.result():
                valid_promos.append(promos_by_code[code])

        return valid_promos
    finally:
        await cs.close()
