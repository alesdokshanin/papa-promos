import re
from dataclasses import dataclass
from typing import List, Optional


@dataclass
class Promo:
    code: str
    name: str

    @property
    def aliases(self) -> List[str]:
        alias_string = self._split_categories()[1]
        return [alias.strip() for alias in alias_string.split(",")]

    @property
    def description(self) -> str:
        return self._split_categories()[2]

    @property
    def condition(self) -> Optional[str]:
        try:
            return self._split_categories()[3]
        except IndexError:
            return None

    @property
    def min_order(self) -> Optional[float]:
        if self.condition is not None:
            match = _min_order_regex.search(self.condition)
            if match is None:
                return None

            matched_value = match.groups()[0].replace(",", ".")
            return float(matched_value)
        return None

    @property
    def locations(self) -> List[str]:
        try:
            city_string = self._split_categories()[4]
            return [city.strip() for city in city_string.split(",")]
        except IndexError:
            return []

    def _split_categories(self) -> List[str]:
        split_pieces = self.name.split("-")
        return [s.strip() for s in split_pieces]


_min_order_regex = re.compile(r"(\d+(.\d+)?)")
