from datetime import datetime
from itertools import groupby, zip_longest
from typing import Iterable, List

from telegram import User

from papabot.cache import redis_client
from papabot.promos import Promo


def grouper(iterable, n, fillvalue=None):
    """Collect data into fixed-length chunks or blocks"""
    # grouper('ABCDEFG', 3, 'x') --> ABC DEF Gxx"
    args = [iter(iterable)] * n
    return zip_longest(*args, fillvalue=fillvalue)


def promo_group_to_human(promo_group: List[Promo]) -> str:
    promo = promo_group[0]

    return "*{condition}* — {description}\n`{codes}`".format(
        condition=escape_markdown(promo.condition),
        description=escape_markdown(promo.description),
        codes=" ".join([promo.code for promo in promo_group]),
    )


def escape_markdown(s) -> str:
    s = s.replace("*", "").replace("_", "")
    return s


def group_promos(promos: Iterable[Promo]) -> List[List[Promo]]:
    def keyfunc(promo: Promo):
        return (promo.description, promo.min_order, promo.condition)

    promos = sorted(promos, key=keyfunc)
    groups = []

    for _, group in groupby(promos, key=keyfunc):
        groups.append(list(sorted(group, key=lambda p: p.code)))

    groups = list(sorted(groups, key=lambda g: g[0].min_order))
    return groups


def serialize_user_info(user: User) -> str:
    return f"{user.id}::{user.username}::{user.full_name}"


def log_users_visit(user: User):
    redis_client.zadd(
        "users", {serialize_user_info(user): int(datetime.now().timestamp())}
    )
