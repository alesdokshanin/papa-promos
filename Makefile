.EXPORT_ALL_VARIABLES:
COMPOSE_FILE ?= docker/docker-compose-local.yml
COMPOSE_PROJECT_NAME ?= papa-promos
DOTENV_FILE ?= .env
DIR = $(shell pwd)
REQS = pyproject.toml poetry.lock
CODE = $(shell find . -type f -name '*.py')

-include $(DOTENV_FILE)

.PHONY: docker-up
docker-up:
	@docker-compose up -d
	@docker-compose ps

.PHONY: docker-down
docker-down:
	@docker-compose down

.PHONY: docker-stop
docker-stop:
	@docker-compose stop

.PHONY: docker-logs
docker-logs:
	@docker-compose logs --follow

.PHONY: lint-black
lint-black:
	@poetry run black --check --diff .

.PHONY: lint-flake8
lint-flake8:
	@poetry run flake8 src tests

.PHONY: lint-isort
lint-isort:
	@poetry run isort --check-only --diff --recursive .

.PHONY: lint-mypy
lint-mypy:
	@poetry run mypy --ignore-missing-imports src tests

.PHONY: lint
lint: lint-black lint-flake8 lint-isort lint-mypy

.PHONY: fmt
fmt:
	@poetry run isort --recursive src
	@poetry run black src

.PHONY: test-integration
test-integration:
	@poetry run py.test \
		--capture=sys \
		-vv \
		tests/integration/

.PHONY: package
package: $(CODE) $(REQS)
	@echo "Building a lambda zip package..."
	docker build -t papabot_builder -f docker/Dockerfile.lambda_builder .
	docker run --rm -v $(DIR)/package:/out -t papabot_builder bash package.sh

.PHONY: clean
clean:
	@echo "Cleaning up..."
	rm -rf build
